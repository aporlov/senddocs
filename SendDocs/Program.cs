﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Configuration;
using System.Net.Configuration;
using System.Net;
using NLog;

namespace SendDocs
{
    class Program
    {
        public static Logger log;
        static string PathDocs;
        static string PathDeleted;
        static string host;
        static string port;
        static string username;
        static string password;
        static string Sender;
        static string SubjectDefault;
        static string BodyDefault;
        static string OptionFile;
        static int MaxDay;
        static Boolean OnlyDirForConfig = false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static int Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.GetEncoding(1251);
            log = LogManager.GetLogger("SendDocs");
            try
            {
                log.Info("Стартуем ");
                ReadConfigFile();
                //1 шаг сканируем файл настроек создаем структуру
                //структура файла настроек 
                //решетка комментарий
                //2030001;aporlov@gmail.com;*.dbf;Накладная SIA
                //2030002;ftp://free:free@ftp.zaupki.gov.ru/nakl;p-*.dbf
                List<OptionLine> OptionLines = GetOptionLines(OptionFile);
                if (OptionLines.Count == 0)
                {
                    log.Error("Main(): Файл настроек неверный. ");
                    //System.Console.WriteLine("Файл настроек неверный");
                    return 1;
                }
                //Читаем структуру каталогов
                List<String> alldir = System.IO.Directory.GetDirectories(PathDocs,"*", System.IO.SearchOption.AllDirectories).ToList();
                // Добавил параметр . Если включен то обрабатываются только каталоги из конфига, остальные игнорируются  
                if (OnlyDirForConfig)
                {
                    log.Info("Установлен параметр OnlyDirForConfig");
                    alldir = alldir.Where(c => OptionLines.Exists(m => c.EndsWith(m.dir) && !m.destination.Contains("nothing"))).ToList();
                }
                log.Info(" Количество сканируемых каталогов-{0}. Количество каталогов в конфиг-файле-{1}", alldir.Count(), OptionLines.Count());
                string nothingrootdir = string.Empty;
                foreach (var dir in alldir)
                {
                    //Разделяем путь на каталоги 
                    string[] buffer = dir.Substring(PathDocs.Length).TrimStart('\\').Split('\\');
                    //ищем в файле настроек есть ли для этого каталога настройка
                    List<OptionLine> seeklines = OptionLines.Where(c =>  buffer.Last<string>().ToString()==c.dir).ToList();
                    List<OptionLine> nothingseeklines = OptionLines.Where(c => buffer.First<string>().ToString() == c.dir&&c.destination=="nothing").ToList();
                    if (nothingseeklines.Count > 0)
                    {
                        log.Trace("Main(): Каталог  {0}  не обрабатываем",  dir);
                        //System.Console.WriteLine(string.Format("{0}: Каталог  {1}  не обрабатываем ", DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss"), dir));
                        continue;
                    }
                    //Если каталог не найден в файле настроек то
                    if (seeklines.Count == 0)
                    {
                        //нужно проверить если корневой каталог с гнастройкой nothing то все внутренние не обрабатываем
                        if (dir.Contains(nothingrootdir)&&!String.IsNullOrEmpty(nothingrootdir))
                        {
                            log.Trace("Main(): Каталог  {0}  не обрабатываем",  dir);
                            //System.Console.WriteLine(string.Format("{0}: Каталог  {1}  не обрабатываем ", DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss"), dir));
                        }
                        else
                        {
                            List<string> filePaths = Directory.GetFiles(dir, "*").ToList();
                            // Try1
                            foreach (var item in filePaths)
                            {
                                try
                                {
                                    File.Copy(item, System.IO.Path.Combine(PathDeleted, System.IO.Path.GetFileName(item)), true);
                                    File.Delete(item);
                                    log.Trace(" Main(): Не найден шаблон файл {0} будет удален",  dir);
                                    System.Console.WriteLine(string.Format("{0}: Не найден шаблон файл {1} будет удален", DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"), dir));
                                }
                                catch (Exception e)
                                {
                                    log.Error("Main(): {1} ",  e.Message);
                                }
                            }
                        }
                    }
                    //Каталог найден . Начинаем обработку
                    foreach (var mask1 in seeklines.GroupBy(x=>x.mask))
                    {   
                        List<string> filePaths = Directory.GetFiles(dir, mask1.Key).ToList();
                        if (filePaths.Count() > 0)
                        {
                            foreach (var line in seeklines.Where(c => c.mask == mask1.Key))
                            {
                                if (line.destination.Contains("nothing"))
                                {
                                    log.Trace("Main(): Каталог  {0}  не обрабатываем",  line.dir);
                                    //System.Console.WriteLine(string.Format("{0}: Каталог  {1}  не обрабатываем ", DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss"), line.dir));
                                }
                                else
                                {
                                    if (line.destination.IndexOf("ftp:") == -1) SendEMAIL(line, ref filePaths);
                                    if (line.destination.IndexOf("ftp:") != -1)
                                    {  
                                        #region if ftp
                                        string[] buff = line.destination.Split(',');
                                        //Внутри могут быть почтовые адреса поэтому 
                                        if (buff.Length > 1)
                                        {
                                            for (int i = 0; i < buff.Length; i++)
                                            {
                                                OptionLine newline = new OptionLine()
                                                {
                                                    body = line.body,
                                                    destination = buff[i],
                                                    dir = line.dir,
                                                    mask = line.mask,
                                                    subject = line.subject
                                                };
                                                if (line.destination.IndexOf("ftp:") == -1) SendEMAIL(newline, ref filePaths);
                                                if (line.destination.IndexOf("ftp:") != -1) PutFTP(newline, ref filePaths);
                                            }
                                        }
                                        else PutFTP(line, ref filePaths);
                                        #endregion
                                    }
                                    
                                }
                            }
                            foreach (var file in filePaths)
                            {
                                File.Delete(file);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Main():  {0} ", e.Message);
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss") + e.Message);
                return 1;
            }
            return 0;
        }

        private static bool isFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException e)
            {
                log.Error("isFileLocked():  {0} ", e.Message);
                return true;
            }
            finally
            {
                if (stream !=null )
                    stream.Close();
            }
            return false;

        }
        private static void ReadConfigFile()
        {
            try
            {
                SmtpSection mailSettings = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                host = mailSettings.Network.Host;
                port = Convert.ToString(mailSettings.Network.Port);
                username = mailSettings.Network.UserName;
                password = mailSettings.Network.Password;
                Sender = ConfigurationManager.AppSettings["Sender"];
                PathDocs = ConfigurationManager.AppSettings["PathDocs"];
                string _PathDeleted = ConfigurationManager.AppSettings["PathDeleted"];
                SubjectDefault = ConfigurationManager.AppSettings["SubjectDefault"];
                BodyDefault = ConfigurationManager.AppSettings["BodyDefault"];
                OptionFile = ConfigurationManager.AppSettings["OptionFile"];
                Int32.TryParse(ConfigurationManager.AppSettings["MaxDay"], out MaxDay);
                PathDeleted = Path.Combine(_PathDeleted,DateTime.Now.Date.ToString("dd.MM.yyyy"));
                // Добавил параметр . Если включен то обрабатываются только каталоги из конфига, остальные игнорируются  
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["OnlyDirForConfig"]))
                {
                    OnlyDirForConfig = Boolean.Parse(ConfigurationManager.AppSettings["OnlyDirForConfig"]);
                }

                //Удаление каталогов со старыми логами 
                if (!Directory.Exists(PathDeleted)) Directory.CreateDirectory(PathDeleted);
                for (int i = MaxDay + 1; i < 20; i++)
                {
                    string[] olddirs = Directory.GetDirectories(_PathDeleted, DateTime.Now.Date.Date.Subtract(new TimeSpan(i, 0, 0, 0)).ToString("dd.MM.yyy"), SearchOption.AllDirectories);
                    foreach (string olddir in olddirs)
                    {
                        if (Directory.Exists(olddir))
                        {
                            Directory.Delete(olddir, true);
                            log.Info("Каталог {0} удален.", olddir);
                            // System.Console.WriteLine(string.Format("Каталог {0} удален", olddir));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ReadConfigFile(): {0} ",  e.Message);
                //System.Console.WriteLine(string.Format("{0}: Error: {1} ", DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"), e.Message));
            }  

        }
        /// <summary>
        /// Отправить почту
        /// </summary>
        /// <param name="line"></param>
        /// <param name="files">Список файлов</param>
        private static void SendEMAIL(OptionLine line,  ref List<string> files)
        {
            List<string> FailedFiles = new List<string>();
            foreach (string file in files)
            {
                try
                {
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(
                       Sender,
                       line.destination,
                       line.subject.Replace("%NumDoc%",Path.GetFileNameWithoutExtension(file)), 
                       line.body);
                    string newnamefile = System.IO.Path.Combine(PathDeleted, System.IO.Path.GetFileName(file)).Replace('р', 'p').Replace('Р', 'P');
                    if (File.Exists(newnamefile))  File.Delete(newnamefile);
                        
                    File.Copy(file,newnamefile , true);
                    Attachment attachment = new Attachment(newnamefile);
                    message.Attachments.Add(attachment);
                    using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient())
                    {
                        client.Send(message);                   
                    }
                    attachment.Dispose();
                    message.Attachments.Dispose();
                    message.Dispose();
                    message = null;
                    log.Info(" Файл {0} отправлен на почту: {1}.", file, line.destination);
                    //System.Console.WriteLine(string.Format("{0}: Файл {1} отправлен на почту: {2}", DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"), file, line.destination));
                }
                catch (Exception e)
                {
                    FailedFiles.Add(file);
                    log.Error("SendEMAIL():  {0} ", e.Message);
                    //System.Console.WriteLine(string.Format("{0}: Error: Файл {1} {2} {3}", DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"), file, line.destination, e.Message));
                }                          
            }
            foreach(var item in FailedFiles) files.Remove(item);
        }
        /// <summary>
        /// Отправка на FTP
        /// </summary>
        /// <param name="line"></param>
        /// <param name="files">Список файлов</param>
        private static void PutFTP(OptionLine line, ref List<string> files)
        {
            List<string> FailedFiles = new List<string>();
            foreach (var file in files)
            {
                FtpWebRequest regFTP = null;
                FtpWebResponse response = null;
                try
                {
                    string newnamefile = System.IO.Path.Combine(PathDeleted, System.IO.Path.GetFileName(file)).Replace('р', 'p').Replace('Р', 'P');
                    File.Copy(file, newnamefile, true);
                    regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(line.destination + System.IO.Path.GetFileName(newnamefile)));
                    regFTP.Method = WebRequestMethods.Ftp.UploadFile;
                    regFTP.UseBinary = true;
                    byte[] bytes = File.ReadAllBytes(newnamefile);
                    regFTP.ContentLength = bytes.Length;
                    var requestStream = regFTP.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    response = (FtpWebResponse)regFTP.GetResponse();
                    if (response != null)
                        response.Close();
                    log.Info(" Файл {0} отправлен на ftp: {1}",  file, line.destination);
                    //System.Console.WriteLine(string.Format("{0}: Файл {1} отправлен на ftp: {2}", DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"), file, line.destination));
                    // Спцециально для Деловой культуры создаем файл флаг и отправляем на фтп
                    if (!String.IsNullOrEmpty(line.subject))
                    {
                        string flagfilename = System.IO.Path.GetFileNameWithoutExtension(newnamefile) + "." + line.subject;
                        regFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(line.destination + System.IO.Path.GetFileName(flagfilename)));
                        regFTP.Method = WebRequestMethods.Ftp.UploadFile;
                        regFTP.UseBinary = true;
                        var flagbyte = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
                        regFTP.ContentLength = flagbyte.Length;
                        requestStream = regFTP.GetRequestStream();
                        requestStream.Write(flagbyte, 0, flagbyte.Length);
                        requestStream.Close();
                        response = (FtpWebResponse)regFTP.GetResponse();
                        if (response != null)
                            response.Close();
                        log.Info(" Файл {0} отправлен на ftp: {1}",  flagfilename, line.destination);
                        //System.Console.WriteLine(string.Format("{0}: Файл флаг {1} отправлен на ftp: {2}", DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"), flagfilename, line.destination));
                    }
                    
                }
                catch (WebException webex)
                {
                    if ((response != null) && (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable))
                    {
                        FailedFiles.Add(file);

                        log.Error("PutFTP(): Файл {0} {1} {2}", file, line.destination, webex.Message);
                        //System.Console.WriteLine(string.Format("{0}: Исключение в программе: Файл {1} {2} {3}", DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss"), file, line.destination, webex.Message));
                    }
                  
                }
            }
            foreach (var item in FailedFiles) files.Remove(item);
        }
        /// <summary>
        /// Читаем файл настроек создаем список 
        /// </summary>
        /// <param name="p">имя файла настроек</param>
        /// <returns></returns>
        private static List<OptionLine> GetOptionLines(string p)
        {
            Encoding encoding = Encoding.GetEncoding(1251);
            List<OptionLine> OptionLines = new List<OptionLine>();
            try
            {
                IEnumerable<string> StringForOptionFile = File.ReadLines(p, encoding);
                int i = 1;
                foreach (var item in StringForOptionFile)
                {
                    try
                    {
                        //Комментарий пропускаем
                        if (item.Trim().IndexOf('#') == 0) continue;
                        //Пустые строки пропускаем
                        if (String.IsNullOrWhiteSpace(item)) continue;
                        string[] buffer = item.Trim().Split(';');
                        if (buffer[0] == null || buffer[1] == null || buffer[2] == null)
                        {
                            log.Error("GetOptionLines():Ошибка в конфиге в строке № : {0}", i.ToString());
                            //System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy HH:mm:ss") + "Ошибка в строке № " + i.ToString());
                            continue;
                        }
                        OptionLine optionline = new OptionLine() { dir = buffer[0], destination = buffer[1], mask = buffer[2] };
                        if (buffer.Length > 3) optionline.subject = buffer[3];
                        else if (optionline.destination.IndexOf("ftp:") == -1) optionline.subject = SubjectDefault;
                        if (buffer.Length > 4) optionline.body = buffer[4];
                        else if (optionline.destination.IndexOf("ftp:") == -1) optionline.body = BodyDefault;
                        OptionLines.Add(optionline);
                    }
                    catch (Exception e)
                    {
                        log.Error("GetOptionLines(): {0} в строке конфига {1} ",  e.Message, item);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("GetOptionLines() {0} ", e.Message);
            }
            return OptionLines;
        }
        //структура файла настроек 
        //решетка комментарий
        //2030001;aporlov@gmail.com;*.dbf;Накладная SIA
        //2030002;ftp://free:free@ftp.zaupki.gov.ru/nakl;p-*.dbf
        public class OptionLine
        {
            public string dir { get; set; }
            public string destination { get; set; }
            public string mask { get; set; }
            public string subject { get; set; }
            public string body { get; set; }
            public string external_prog { get; set; }
        }

    }
}
